package oodp.miniproj1.clock;

import oodp.miniproj1.counter.SettableCounterType;
import oodp.miniproj1.counter.SixtyCounter;
import oodp.miniproj1.counter.TwentyFourCounter;
import oodp.miniproj1.time.Time;
import oodp.miniproj1.time.TimeType;

public class ChessClock implements ClockType {

	private SettableCounterType hours = new TwentyFourCounter();
	private SettableCounterType minutes = new SixtyCounter(hours);
	private SettableCounterType seconds = new SixtyCounter(minutes);

	public ChessClock() {
		this.hours.setCount(0);
		this.minutes.setCount(0);
		this.seconds.setCount(0);

	}

	public ChessClock(Time time) {
		this.hours.setCount(time.getHour());
		this.minutes.setCount(time.getMinute());
		this.seconds.setCount(time.getSecond());
	}

	@Override
	public void tickTack() {

		seconds.count();

	}

	@Override
	public void setTime(TimeType time) {

		this.hours.setCount(time.getHour());
		this.minutes.setCount(time.getMinute());
		this.seconds.setCount(time.getSecond() - 1);

	}

	@Override
	public TimeType getTime() {

		return new Time(hours.getCount(), minutes.getCount(), seconds.getCount());
	}

	public String toString() {

		return String.format("%s", this.getTime().toString());
	}

}
