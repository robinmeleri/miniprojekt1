package oodp.miniproj1.counter;

public class TwentyFourCounter extends SettableCounter {
	
	public TwentyFourCounter () {
		
		this(null);
	}
	
	public TwentyFourCounter(CounterType next) {
		
		super(24,next,Direction.DECREASING);
	}

}
