package oodp.miniproj1.clock;

import oodp.miniproj1.time.TimeType;

public interface ClockType
  {
  public void tickTack();
  public void setTime(TimeType time);
  public TimeType getTime();
  public String toString();
  }
