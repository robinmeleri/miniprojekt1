package oodp.miniproj1.GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import oodp.miniproj1.clock.ChessClock;
import oodp.miniproj1.controller.Controller;
import oodp.miniproj1.functions.SetTime;
import oodp.miniproj1.time.Time;

public class GUI_WAC {

	JPanel panel;
	JFrame frame = new JFrame();
	JPanel clockPanel;
	JPanel digClockPanel;
	JTabbedPane tabedPane;
	JTabbedPane tabedPane1;
	Time time = new Time("02:05:05");
	ChessClock clock = new ChessClock(time);
	Controller controller = new Controller(clock);
	AnalogueClock analogueClock = new AnalogueClock(controller);
//	AnalogueClock secondAnaolgueClock = new AnalogueClock(controller);
	SetTime setClockTime = new SetTime(controller);
	

	public GUI_WAC() {


		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		tabedPane = new JTabbedPane();
		tabedPane1 = new JTabbedPane();
		tabedPane.add("Player one", analogueClock);
	// tabedPane1.add("Player two", secondAnaolgueClock);
		clockPanel = new JPanel(new GridLayout(1, 2));
		clockPanel.add(tabedPane);
		clockPanel.add(tabedPane1);

		panel.add(BottomInputPanel(), BorderLayout.SOUTH);
		panel.add(clockPanel);

		this.frame.setSize(800, 500);
		this.frame.add(panel);
		this.frame.setVisible(true);
	}

	/* public JPanel TopInputPanel() {
		JPanel topInputPanel = new JPanel();
		topInputPanel.setLayout(new GridLayout(1, 2));

		return topInputPanel;
	} */ 

	public JPanel BottomInputPanel() {

		JPanel bottomInputPanel = new JPanel();
		bottomInputPanel.setLayout(new GridLayout(1, 2));
		bottomInputPanel.add(setClockTime.setTime());

		return bottomInputPanel;
	}

	public static void main(String args[]) {
		new GUI_WAC();

	}

}
