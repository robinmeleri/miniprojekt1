package oodp.miniproj1.GUI;



import javax.swing.*;

import oodp.miniproj1.controller.Controller;
import oodp.miniproj1.time.TimeType;
import java.awt.Graphics;

import java.awt.*;

public class AnalogueClock extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double hourAngle;
	private double minuteAngle;
	private double secondAngle;
	private Timer timer;
	private Controller controller;

	public AnalogueClock(Controller controller) {
		this.controller = controller;
		setBackground(Color.white);
		setPreferredSize(new Dimension(25, 25));
		timer = new Timer(1000, e -> doTick());
		timer.start();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		TimeType time = this.controller.getTime();
		// System.out.println(time.toString());
		int hours = time.getHour();
		int minutes = time.getMinute();
		int seconds = time.getSecond();
		secondAngle = seconds * (2 * Math.PI / 60) - Math.PI / 2;
		minuteAngle = minutes * (2 * Math.PI / 60) - Math.PI / 2;
		hourAngle = hours * (2 * Math.PI / 12) - Math.PI / 2;
		int width = getWidth();
		int height = getHeight();
		int x = width / 2;
		int y = height / 2;
		g.drawOval(0, 0, width, height);
		double yp = Math.sin(secondAngle);
		double xp = Math.cos(secondAngle);
		double yp1 = Math.sin(minuteAngle);
		double xp1 = Math.cos(minuteAngle);
		double yp2 = Math.sin(hourAngle);
		double xp2 = Math.cos(hourAngle);
		g.setColor(Color.red);
		g.drawLine(x, y, x + (int) (xp * x), y + (int) (yp * y)); // Second hand
		g.setColor(Color.blue);
		g.drawLine(x, y, x + (int) (xp1 * x) / 2, y + (int) (yp1 * y) / 2); // Minute hand
		g.setColor(Color.black);
		g.drawLine(x, y, x + (int) (xp2 * x) / 4, y + (int) (yp2 * y) / 4); // Hour hand

	}

	public void doTick() {
	

			this.controller.tickTack();
			repaint();
			System.out.println(controller.getTime().toString());

	}

}
