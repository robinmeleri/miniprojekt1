package oodp.miniproj1.controller;
import oodp.miniproj1.clock.ClockType;
import oodp.miniproj1.time.Time;
import oodp.miniproj1.time.TimeType;

public class Controller {

	ClockType clock;
	private Time time = new Time("00:00:00");


	public Controller(ClockType clock) {

		this.clock = clock;
	}

	public void tickTack() {

		if(clock.getTime() != time ) {
		clock.tickTack();
		}
	}

	public void setTime(TimeType time) {

		clock.setTime(time);
	}


	public TimeType getTime() {

		return clock.getTime();
	}

	public String toString() {

		return clock.toString();
	}
	

}
