package oodp.miniproj1.counter;

public abstract class AbstractCounter implements CounterType {

	public enum Direction {
		INCREASING, DECREASING
	};

	protected int countedValue;

	private boolean isPaused;

	private final boolean IS_CIRCULAR;

	private final int COUNT_SPACE;

	private Direction direction;

	private CounterType nextCounter;

	public AbstractCounter(int countSpace, CounterType nextCounter, Direction direction) {

		if (countSpace >= 2)

		{

			this.COUNT_SPACE = countSpace;
			this.IS_CIRCULAR = true;

		}

		else {
			this.COUNT_SPACE = 0;
			this.IS_CIRCULAR = false;
		}

		this.direction = direction;
		this.nextCounter = nextCounter;
	}

	public void setDirection(Direction direction) {

		this.direction = direction;

	}

	@Override
	public int getCount() {

		return this.countedValue;

	}

	@Override
	public void reset() {

		this.countedValue = 0;

	}

	@Override
	public void pause() {

		isPaused = true;

	}

	@Override
	public void resume() {

		isPaused = false;

	}

	@Override
	public void count() {

		if (!isPaused) {
			if (direction == Direction.INCREASING) {
				countedValue++; //
			//	System.out.println(countedValue);

				if (IS_CIRCULAR && countedValue == COUNT_SPACE - 1) { //
					//System.out.println(COUNT_SPACE);

					this.countedValue = 0;

					if (nextCounter != null) {
						this.nextCounter.count();

					}
				}
			}

			else if (direction == Direction.DECREASING) {
				countedValue--;
				// System.out.println(getCount());
				if (IS_CIRCULAR && countedValue == 0) {

					this.countedValue = this.COUNT_SPACE -1 ;

					if (nextCounter != null) {
						this.nextCounter.count();

					}

				}

			}

		//	System.out.println(countedValue);
		}
	}

	@Override
	public String toString() {

		return String.format("%02d", countedValue);

	}

}
