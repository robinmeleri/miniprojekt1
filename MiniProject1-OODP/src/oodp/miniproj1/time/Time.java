package oodp.miniproj1.time;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Time implements TimeType {
	private int second, minute, hour;

	public Time(int hour, int minute, int second) {
		setHour(hour);
		setMinute(minute);
		setSecond(second);
	}
	

	public Time(String time) {
		// kolla format Hh:Mm:Ss (H: 0-2, h: 0-9, M: 0-5, m: 0-9, S: 0-5, s: 0-9)
		Pattern plainPattern = Pattern.compile("^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$");
		Matcher plainMatcher = plainPattern.matcher(time);
		// kolla format Day Hh:Mm:Ss (Day: Mon, Tue, ..., H: 0-2, h: 0-9, M: 0-5, m:
		// 0-9, S: 0-5, s: 0-9)

		if (plainMatcher.find()) {
			String[] parts = time.split(":");
			setHour(Integer.parseInt(parts[0]));
			setMinute(Integer.parseInt(parts[1]));
			setSecond(Integer.parseInt(parts[2]));
		}

		else
			throw new RuntimeException("Illegal time format: " + time);
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second % 60;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute % 60;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour % 24;
	}


	public String toString() {
		char[] array = new char[8];
		array[0] = (char) ('0' + hour / 10);
		array[1] = (char) ('0' + hour % 10);
		array[2] = ':';
		array[3] = (char) ('0' + minute / 10);
		array[4] = (char) ('0' + minute % 10);
		array[5] = ':';
		array[6] = (char) ('0' + second / 10);
		array[7] = (char) ('0' + second % 10);
		return new String(array);

	}
}
