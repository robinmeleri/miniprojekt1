package oodp.miniproj1.functions;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import oodp.miniproj1.controller.Controller;
import oodp.miniproj1.time.Time;
import oodp.miniproj1.time.TimeType;

public class SetTime {

	private Controller controller;

	public SetTime(Controller controller) {

		this.controller = controller;
	}

	public JPanel setTime() {

		JPanel timePanel = new JPanel();
		JTextField timeText = new JTextField(8);
		timeText.setFont(new Font("Calibri", 10, 18));
		timeText.setText("Hour:Min:Sec");
		JButton timeButton = new JButton("Set Time");

		timeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

					TimeType time = new Time(timeText.getText());
					controller.setTime(time);
				

			}

		});

		timePanel.add(timeText);
		timePanel.add(timeButton);

		return timePanel;
	}

}
