package oodp.miniproj1.counter;

public class SixtyCounter extends SettableCounter {
	
	public SixtyCounter () {
		
		this(null);
	}

	public SixtyCounter(CounterType next) {
		
		super(60,next, Direction.DECREASING);
		
		
	}
	
	 

}
