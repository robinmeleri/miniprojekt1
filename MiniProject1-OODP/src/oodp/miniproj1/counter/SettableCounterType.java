package oodp.miniproj1.counter;

public interface SettableCounterType extends CounterType
{
public void setCount(int value);
}
